﻿namespace ThreadStatic
{
    internal class Custom
    {
        public int AccessCounter;

        public Custom(int init = 0)
        {
            AccessCounter = init;
        }

        public void Add()
        {
            AccessCounter++;
        }
    }
}